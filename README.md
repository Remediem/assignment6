# Sign Language Translator using React
Attempt to create a sign language translator using React.

## Background
Noroff assignment

## Getting started
Clone the project.
cd into project.
run `npm install` to install all modules.
run `npm start` and go to the host url.

## Usage
Go to the host url. Provide a username.
Click on the login button, and you will be taken to the translation page. Type in a sentence to translate, and click the translate button. Navigating to the profile page via th nav bar will show you your last 10 translations.
You can also visit a hosted version of this project on:
`https://damp-springs-42687.herokuapp.com/`.

## Authors
Sigurd Skaga
