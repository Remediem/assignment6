export interface ResponseError
{
    message: string | null
}