export interface UserResponse extends User
{
    id: number
}

export interface User
{
    username: string
    translations: string[]
}