import { useNavigate } from "react-router-dom";
import LoginForm from "../../components/LoginForm/LoginForm";
import "./Login.css";

function Login()
{
    const navigate = useNavigate();

    // Function to navigate to translation page when logging in.
    function handleLogin(_username: string)
    {
        navigate("translation");
    } 

	return (
        <div id="login-container-1">
            <div id="login-container-2">
                <div id="login-welcome">
                    <img id="logo" src="resources/Logo-Hello.png" alt="Robot Hello welcome message." /> 
                    <span>Lost in Translation</span> <br/>
                    <span>Get started</span>
                </div>
                <div id="form-container">
                    <LoginForm onLogin={handleLogin}></LoginForm>
                </div>
            </div>
        </div>
    );
}

export default Login;