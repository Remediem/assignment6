import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import TranslationForm from "../../components/TranslationForm/TranslationForm";
import TranslationView from "../../components/TranslationView/TranslationView";
import { readUser, saveUser } from "../../services/storage.service";
import { updateUser } from "../../services/user.service";
import "./Translation.css";



function Translation()
{
    const navigate = useNavigate();
    const [translationInput, setTranslationInput] = useState<string>("");

    useEffect(() => {
        // If user not in localStorage, navigate to login page.
        if (readUser() === null)
        {
            navigate("/");
        }
    }, [translationInput]);

    // Function to handle translation input. Updates user translation history in localStorage and Trivia API.
    async function handleTranslate(_translationInput: string)
    {
        setTranslationInput(_translationInput);

        const user = readUser();
        if (user)
        {
            const response = user.translations.find((_translation: string) => _translation === _translationInput);
            if (response === undefined)
            {
                user.translations.push(_translationInput);

                // Update localStorage and Trivia API
                saveUser(user);
                await updateUser(user.id, user);
            }
        }
        
    } 
    
    return (
    <div id="translation-container-1">
        <div id="translation-container-2">
            <div id="translation-container-3">
                <TranslationForm onTranslate={handleTranslate}></TranslationForm>
            </div>
            <div id="translation-container-4">
                <TranslationView translationInput={translationInput}></TranslationView>
            </div>
        </div>
    </div>
    );
}

export default Translation;