import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import TranslationHistory from "../../components/TranslationHistory/TranslationHistory";
import { readUser } from "../../services/storage.service";
import "./Profile.css";

function Profile()
{
    const navigate = useNavigate();

    useEffect(() => {
        // If user not in localStorage, navigate to login page.
        if (readUser() === null)
        {
            navigate("/");
        }
    }, []);

    return (
        <div id="profile-container-1">
            <div id="profile-container-2">
                <TranslationHistory></TranslationHistory>
            </div>
        </div>
    );
}

export default Profile;