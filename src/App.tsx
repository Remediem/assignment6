import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Translation from "./pages/Translation/Translation";
import Profile from "./pages/Profile/Profile";
import Login from "./pages/Login/Login";
import Navbar from "./components/Navbar/Navbar";


// App component
function App() {
	return (
		// Routing
		<BrowserRouter>
				<Navbar></Navbar>
				<Routes>
					<Route path="/" element={<Login />} />
					<Route path="translation" element={<Translation />} />
					<Route path="profile" element={<Profile />} />
				</Routes>
		</BrowserRouter>
	);
}

export default App;
