import { UserResponse } from "../models/user.model";

const USER_STORAGE_KEY = "user";

// Function to save user to localStorage
export function saveUser(_user: UserResponse): void
{
    localStorage.setItem(USER_STORAGE_KEY, JSON.stringify(_user));
}

// Function to read user from loacalStorage
export function readUser(): UserResponse | null
{
    const data = localStorage.getItem(USER_STORAGE_KEY);
    if (data)
    {
        const user = JSON.parse(data); // I dont know why saving as array in the beginning and as object later
        if (Array.isArray(user))
            return user[0];
        return user;
    }
    return null;

}

// Function to delete user from localStorage
export function deleteUser(): void
{
    localStorage.removeItem(USER_STORAGE_KEY);
}