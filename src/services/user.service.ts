import { ResponseError } from "../models/error.model";
import { User } from "../models/user.model";

const TRANSLATION_API = process.env.REACT_APP_TRANSLATION_API ?? "";
const API_KEY = process.env.REACT_APP_API_KEY ?? "";

// Function to create user in Translation API.
export async function createUser(_user: User)
{
    const headers = {
        "Content-Type": "application/json",
        "x-api-key": API_KEY
    };

    try
    {
        const response = await fetch(TRANSLATION_API, 
            {
                method: "POST",
                headers: headers,
                body: JSON.stringify({
                username: _user.username,
                translations: _user.translations
            })});

        const error: ResponseError = {message: null};
        return {error: error, data: [await response.json()]};
    }
    catch (_err: any)
    {
        const error: ResponseError = {message: _err.message};
        return {error: error, data: []};
    }
}

// Function to get user from Trivia API by username.
export async function getUser(_username: string)
{
    try
    {
        const response = await fetch(`${TRANSLATION_API}?username=${_username}`);
        const error: ResponseError = {message: null};
        return {error, data: await response.json()};
    }
    catch (_err: any)
    {
        const error: ResponseError = {message: _err.message};
        return {error, data: []};
    }
}

// Function to update user of particular id with new values.
export async function updateUser(_id: number, _user: User)
{
    const headers = {
        "Content-Type": "application/json",
        "x-api-key": API_KEY
    };

    try 
    {
        const response = await fetch(`${TRANSLATION_API}/${_id}`, {
            method: "PATCH",
            headers: headers,
            body: JSON.stringify({
                username: _user.username,
                translations: _user.translations,
            }),
        });
       
        const error: ResponseError = {message: null};
        return {error, data: await response.json()};
    } 
    catch (_err: any) 
    {
        const error: ResponseError = {message: _err.message};
        return {error, data: []};
    }
}