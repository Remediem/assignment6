import { SyntheticEvent, useState } from "react";
import "./TranslationForm.css";


interface TranslationFormProps
{
    onTranslate: (_translationInput: string) => void
}

function TransitionForm(_props: TranslationFormProps)
{
    const [translationInput, setTranslationInput] = useState<string>("");

    // Get input and update local state
    function onInputChange(_event: SyntheticEvent)
    {
        const input = _event.target as HTMLInputElement;
        setTranslationInput(input.value.trim());
    }

    // Emit translation input to parent
    function onFormSubmit(_event: SyntheticEvent)
    {
        _event.preventDefault();

        if (translationInput !== "")
        {
            _event.preventDefault();
            _props.onTranslate(translationInput);
            const input = _event.target as HTMLInputElement;
            input.value = "";
        }
    }


    return (
        <form id="translation-form" onSubmit={onFormSubmit}>
            <input id="translation-input" type="text" placeholder="Type something to translate...?" onChange={onInputChange}/>
            <button id="translation-submit-button" type="submit">Translate</button>
        </form>
    );
}

export default TransitionForm;