import { useState, useEffect } from "react";
import { readUser, saveUser } from "../../services/storage.service";
import { updateUser } from "../../services/user.service";
import "./TranslationHistory.css";

function TranslationHistory()
{
    const [shouldClearHistory, setShouldClearHistory] = useState(false);

    useEffect(() => 
    {
        if (shouldClearHistory)
        {
            // Update translation history in localStorage and Trivia API
            setShouldClearHistory(false);
            const user = readUser();
            if (user)
            {
                user.translations = [];

                // Update localStorage and Trivia API
                saveUser(user);
                updateUser(user.id, user);
            }
        }
    }, [shouldClearHistory]);

    // Function to get translation history from localStorage.
    function getHistory()
    {
        const user = readUser();
        if (user)
        {
            return user.translations;
        }
        return [];
    }

    function clearHistory()
    {
        setShouldClearHistory(true);
    }

    return (
        <div id="translation-history-container-1">
            <h2>Translation History</h2>
            <ol>
                {/* Get last 10 translations from history and map to <li> elements</li> */}
                {getHistory().reverse().slice(0, 10).map((_translation: string, _idx: number) => (<li key={_idx}>{_translation}</li>))}
            </ol>
            <button onClick={clearHistory}>Clear history</button>
        </div>
    );
}

export default TranslationHistory;