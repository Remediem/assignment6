import { SyntheticEvent,  useEffect,  useState } from "react";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../contexts/UserContext";
import { User } from "../../models/user.model";
import { readUser, saveUser } from "../../services/storage.service";
import { createUser, getUser } from "../../services/user.service";
import "../LoginForm/LoginForm.css";



interface LoginFormProps
{
    onLogin: (_username: string) => void
}

function LoginForm(_props: LoginFormProps)
{  
    const [username, setUsername] = useState<string>("");
    const { user, login } = useUser();
    const navigate = useNavigate();

    useEffect(() => {
        
        // If user in localStorage, navigate to translation page.
        const user = readUser();
        if (user)
        {
            navigate("translation");
        }
    }, [user]);

    // Submit login form. If valid username, create/get user and log in.
    async function onFormSubmit(_event: SyntheticEvent)
    {
        _event.preventDefault();

        // Validate username
        if (username !== "")
        {
            let response = await getUser(username);
        
            // If no user by that name, create new user
            if (response.data.length === 0)
            {

                const user: User = {
                    username: username,
                    translations: []
                };
                response = await createUser(user);
            }
            
            // Save user to localStorage and log in
            if (response.error.message === null)
            {
                saveUser(response.data);
                login(username);
                _props.onLogin(username); 
            }
        }
    }

    // Get username and update local state
    function onUsernameChange(_event: SyntheticEvent)
    {
        const input = _event.target as HTMLInputElement;
        setUsername(input.value.trim());
    }

    return (
        <form id="login-form" onSubmit={onFormSubmit}>
            <input id="username-input" type="text" placeholder="What's your name?" onChange={onUsernameChange}/>
            <button id="username-submit-button" type="submit">Login</button>
        </form>
    );
}

export default LoginForm;