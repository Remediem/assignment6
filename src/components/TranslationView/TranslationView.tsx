import "./TranslationView.css";

interface TranslationViewProps
{
    translationInput: string
}


function TranslationView(_props: TranslationViewProps)
{
    return (
        <div id="translation-view">
            {/* Split translation input into individual letters and map to <img> elements. */}
            {_props.translationInput.split(" ").map((word) => word.trim()).join("").split("").map((_letter, _idx) => (<img src={`resources/individial_signs/${_letter}.png`} alt={_letter} key={_idx}></img>))}
        </div>
    );
}
export default TranslationView;