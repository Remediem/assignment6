import "./Navbar.css";
import { useLocation, useNavigate } from "react-router-dom";
import { deleteUser } from "../../services/storage.service";

function Navbar()
{

    const location = useLocation();
    const navigate = useNavigate();

    // Function to delete user from localStorage and navigate to login page, on logout.
    function onLogout()
    {
        deleteUser();
        navigate("/");

    }

    return (
        (location.pathname != "/") ? 
        <nav>
            <div><a onClick={onLogout}><u>Logout</u></a></div>
            {(location.pathname != "/translation") ? <div><a href="/translation">Translation</a></div> : <></>}
            {(location.pathname != "/profile") ? <div><a href="/profile">Profile</a></div> : <></>}
        </nav> : <></>
    );
}

export default Navbar;