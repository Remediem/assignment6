import { createContext, useContext, useState } from "react";
import { User } from "../models/user.model";


interface UserProviderProps
{
    children: React.ReactNode
}

interface UserState
{
    user: User
    login: (_username: string) => void
    logout: () => void
}

const defaultState: UserState = {
    user: {
        username: "",
        translations: []
    },
    login: (_username: string) => {return;},
    logout: () => {return;}
};


const UserContext = createContext<UserState>(defaultState);

// Function to use context.
export function useUser()
{
    return useContext(UserContext);
}

// Function to provide context state.
function UserProvider(_props: UserProviderProps)
{
    const [user, setUser] = useState<User>(defaultState.user);

    function login(_username: string) {setUser({username: _username, translations: []});}
    function logout(){setUser(defaultState.user);}
    return <UserContext.Provider value={{user, login, logout}}>{_props.children}</UserContext.Provider>;
}

export default UserProvider;